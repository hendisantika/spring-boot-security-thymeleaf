INSERT INTO role (role_name)
VALUES ('ADMIN');
INSERT INTO role (role_name)
VALUES ('USER');

INSERT INTO user (confirm_password, date_of_birth, email, first_name, gender, image, last_name, password, user_name)
VALUES ('$2y$12$tVCQCFqzn/JP7hXddSHL6O3Z4lyhPbt5YJttCVRI8mOnIEP2vuImm', '1977-09-09', 'admin@yahoo.com', 'Hatake',
        'MALE',
        NULL, 'Kakashi', '$2a$10$7lv7PzqN0dlxmmXiKZFzFO9pN0HDApRDAtrwpgt2Af7mxRuCpvbn.', 'admin');

INSERT INTO user_role (user_id, role_id)
VALUES (1, 1);