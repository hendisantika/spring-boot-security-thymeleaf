CREATE TABLE role
(
    id        bigint NOT NULL auto_increment,
    role_name varchar(255),
    PRIMARY KEY (id),
    UNIQUE KEY (role_name)
);

CREATE TABLE user
(
    id               bigint NOT NULL auto_increment,
    confirm_password varchar(255),
    date_of_birth    datetime,
    email            varchar(255),
    first_name       varchar(255),
    gender           varchar(255),
    image            longblob,
    last_name        varchar(255),
    password         varchar(255),
    user_name        varchar(255),
    PRIMARY KEY (id),
    UNIQUE KEY (user_name)
);

CREATE TABLE user_role
(
    user_id bigint NOT NULL,
    role_id bigint NOT NULL,
    PRIMARY KEY (user_id, role_id)
);

CREATE TABLE rememberme_token
(
    series      varchar(255),
    user_name   varchar(255),
    token_value varchar(255),
    date        datetime,
    PRIMARY KEY (series)
);

ALTER TABLE user_role
    ADD CONSTRAINT FKa68196081fvovjhkek5m97n3y FOREIGN KEY (role_id) REFERENCES role (id);
ALTER TABLE user_role
    ADD CONSTRAINT FK859n2jvi8ivhui0rl0esws6o FOREIGN KEY (user_id) REFERENCES user (id);