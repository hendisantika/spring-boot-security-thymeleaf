package com.hendisantika.springbootsecuritythymeleaf.controller;

import com.hendisantika.springbootsecuritythymeleaf.entity.Role;
import com.hendisantika.springbootsecuritythymeleaf.model.UserDetailForm;
import com.hendisantika.springbootsecuritythymeleaf.model.UserEditForm;
import com.hendisantika.springbootsecuritythymeleaf.service.UserService;
import com.hendisantika.springbootsecuritythymeleaf.util.DBEnumUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.rememberme.PersistentTokenBasedRememberMeServices;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-security-thymeleaf
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 06/05/20
 * Time: 08.13
 */
@Controller
public class UserController {
    @Autowired
    PersistentTokenBasedRememberMeServices persistentTokenBasedRememberMeServices;
    @Autowired
    private UserService userService;

    @GetMapping(value = {"/list"})
    public String listUsers(ModelMap model) {
        List<UserEditForm> users = userService.findAllUsers();
        model.addAttribute("users", users);
        return "userslist";
    }

    @GetMapping(value = {"/delete/{userName}"})
    public String deleteUser(@PathVariable String userName) throws Exception {
        userService.deleteUserByUserName(userName);
        return "redirect:/list";
    }

    @GetMapping(value = {"/", "/welcome"})
    public String welcome(Model model) {
        return "welcome";
    }

    @GetMapping(value = {"/edit/{userName}"})
    public String edit(Model model, @PathVariable String userName) throws Exception {
        UserEditForm userEditForm = userService.findUserForEdit(userName);
        model.addAttribute("userEditForm", userEditForm);
        return "edit";
    }

    @PostMapping(value = "/edit/{userName}")
    public String editUser(@ModelAttribute("userEditForm") @Valid UserEditForm userEditForm,
                           BindingResult bindingResult, Model model) throws Exception {
        if (bindingResult.hasErrors()) {
            return "edit";
        }
        userService.update(userEditForm);
        return "redirect:/welcome";
    }

    @PostMapping(value = "/registration")
    public String registerUser(@ModelAttribute("userForm") @Valid UserDetailForm userForm, BindingResult bindingResult,
                               Model model) throws Exception {
        if (bindingResult.hasErrors()) {
            return "registration";
        }

        Role role = new Role(DBEnumUtil.Role.USER.toString());
        userForm.setRole(role);
        userService.save(userForm);
        model.addAttribute("message", "User " + userForm.getUserName() + " registered successfully. Please login.");
        return "login";
    }

    @GetMapping(value = "/registration")
    public String registration(Model model) {
        model.addAttribute("userForm", new UserDetailForm());
        return "registration";
    }

    @GetMapping(value = "/login")
    public String login(Model model, String error, String logout) {
        if (error != null)
            model.addAttribute("error", "Bad credentials.");

        if (logout != null)
            model.addAttribute("message", "User logged out successfully.");

        return "login";
    }

    @GetMapping(value = "/logout")
    public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            persistentTokenBasedRememberMeServices.logout(request, response, auth);
            SecurityContextHolder.getContext().setAuthentication(null);
        }
        return "redirect:/login?logout";
    }
}
