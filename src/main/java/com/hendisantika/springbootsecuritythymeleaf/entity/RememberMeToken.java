package com.hendisantika.springbootsecuritythymeleaf.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-security-thymeleaf
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 05/05/20
 * Time: 20.30
 */
@Entity
@Table(name = "rememberme_token")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RememberMeToken {

    @Id
    private String series;
    private String userName;
    private String tokenValue;
    private Date date;
}
