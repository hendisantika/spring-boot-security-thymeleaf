package com.hendisantika.springbootsecuritythymeleaf.util;

import com.hendisantika.springbootsecuritythymeleaf.model.UserDetailForm;
import com.hendisantika.springbootsecuritythymeleaf.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-security-thymeleaf
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 05/05/20
 * Time: 20.57
 */
@Component
public class UniqueValueCheckerImpl implements UniqueValueChecker {

    @Autowired
    private UserService userService;

    //TODO:: Based on fieldName can check different unique fields like user,email
    public boolean isFieldValueUnique(String fieldName, String fieldValue) {
        UserDetailForm user = userService.findByUserName(fieldValue);
        return user == null;
    }
}
