package com.hendisantika.springbootsecuritythymeleaf.util;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-security-thymeleaf
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 05/05/20
 * Time: 20.55
 */
public class DBEnumUtil {
    public enum Gender {
        MALE, FEMALE
    }

    public enum Role {
        USER, ADMIN
    }
}
