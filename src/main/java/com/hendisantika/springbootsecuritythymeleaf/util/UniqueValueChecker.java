package com.hendisantika.springbootsecuritythymeleaf.util;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-security-thymeleaf
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 05/05/20
 * Time: 20.56
 */
public interface UniqueValueChecker {

    boolean isFieldValueUnique(String fieldName, String fieldValue);
}
