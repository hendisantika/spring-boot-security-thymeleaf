package com.hendisantika.springbootsecuritythymeleaf.validator;

import com.hendisantika.springbootsecuritythymeleaf.util.UniqueValueChecker;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-security-thymeleaf
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 05/05/20
 * Time: 20.44
 */
public class UniqueValueValidator implements ConstraintValidator<UniqueValue, String> {

    @Autowired
    private UniqueValueChecker uvt;

    private String fieldName;

    @Override
    public void initialize(UniqueValue constraintAnnotation) {
        this.fieldName = constraintAnnotation.fieldName();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return uvt.isFieldValueUnique(fieldName, value);
    }
}
