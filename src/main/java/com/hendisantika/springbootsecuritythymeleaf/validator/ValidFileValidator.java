package com.hendisantika.springbootsecuritythymeleaf.validator;

import org.springframework.web.multipart.MultipartFile;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-security-thymeleaf
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 05/05/20
 * Time: 20.47
 */
public class ValidFileValidator implements ConstraintValidator<ValidFile, MultipartFile> {

    private String[] contentType;
    private long size;

    @Override
    public void initialize(ValidFile constraintAnnotation) {
        this.contentType = constraintAnnotation.contentType();
        this.size = constraintAnnotation.size();
    }

    @Override
    public boolean isValid(MultipartFile value, ConstraintValidatorContext context) {
        boolean validFile = false;
        if (value == null || value.isEmpty()) {
            return validFile;
        }
        //validate content-type and size
        if ((Arrays.asList(contentType).contains(value.getContentType())) && ((value.getSize() / 1024)) <= size) {
            validFile = true;
        }
        return validFile;
    }
}
