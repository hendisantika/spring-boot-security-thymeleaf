package com.hendisantika.springbootsecuritythymeleaf.repository;

import com.hendisantika.springbootsecuritythymeleaf.entity.RememberMeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.authentication.rememberme.PersistentRememberMeToken;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-security-thymeleaf
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 06/05/20
 * Time: 07.52
 */
@Repository
public class CustomPersistentTokenRepositoryImpl implements PersistentTokenRepository {

    @Autowired
    private RememberMeTokenRepository rememberMeTokenRepository;

    @Override
    public void createNewToken(PersistentRememberMeToken token) {
        RememberMeToken rmToken = new RememberMeToken();
        rmToken.setSeries(token.getSeries());
        rmToken.setUserName(token.getUsername());
        rmToken.setTokenValue(token.getTokenValue());
        rmToken.setDate(token.getDate());
        rememberMeTokenRepository.save(rmToken);
    }

    @Override
    public void updateToken(String series, String tokenValue, Date lastUsed) {
        RememberMeToken existingToken = this.rememberMeTokenRepository.findBySeries(series);
        if (existingToken != null) {
            existingToken.setTokenValue(tokenValue);
            existingToken.setDate(lastUsed);
            rememberMeTokenRepository.save(existingToken);
        }
    }

    @Override
    public PersistentRememberMeToken getTokenForSeries(String seriesId) {
        PersistentRememberMeToken persistentRememberMeToken = null;
        RememberMeToken existingToken = this.rememberMeTokenRepository.findBySeries(seriesId);
        if (existingToken != null) {
            persistentRememberMeToken = new PersistentRememberMeToken(existingToken.getUserName(),
                    existingToken.getSeries(), existingToken.getTokenValue(), existingToken.getDate());
        }
        return persistentRememberMeToken;
    }


    @Override
    public void removeUserTokens(String username) {
        List<RememberMeToken> tokens = rememberMeTokenRepository.findByUserName(username);
        rememberMeTokenRepository.deleteAll(tokens);
    }

}
