package com.hendisantika.springbootsecuritythymeleaf.repository;

import com.hendisantika.springbootsecuritythymeleaf.entity.RememberMeToken;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-security-thymeleaf
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 06/05/20
 * Time: 07.52
 */
public interface RememberMeTokenRepository extends JpaRepository<RememberMeToken, String> {
    List<RememberMeToken> findByUserName(String userName);

    RememberMeToken findBySeries(String series);
}
