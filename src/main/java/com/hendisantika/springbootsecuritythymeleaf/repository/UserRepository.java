package com.hendisantika.springbootsecuritythymeleaf.repository;

import com.hendisantika.springbootsecuritythymeleaf.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-security-thymeleaf
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 06/05/20
 * Time: 07.56
 */
public interface UserRepository extends JpaRepository<User, Long> {
    User findByUserName(String userName);
}
