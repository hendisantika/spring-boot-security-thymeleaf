package com.hendisantika.springbootsecuritythymeleaf.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-security-thymeleaf
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 05/05/20
 * Time: 20.33
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RoleForm {
    private String roleName;
}
