package com.hendisantika.springbootsecuritythymeleaf.model;

import com.hendisantika.springbootsecuritythymeleaf.util.DBEnumUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-security-thymeleaf
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 05/05/20
 * Time: 21.02
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserEditForm {
    private String userName;
    @Size(min = 1, max = 10)
    private String firstName;
    @Size(min = 1, max = 10)
    private String lastName;
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    @NotNull
    @Past
    private Date dateOfBirth;
    @NotNull
    private DBEnumUtil.Gender gender;
    private String base64EncodedImage;
}
