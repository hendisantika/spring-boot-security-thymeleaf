package com.hendisantika.springbootsecuritythymeleaf.model;

import com.hendisantika.springbootsecuritythymeleaf.entity.Role;
import com.hendisantika.springbootsecuritythymeleaf.util.DBEnumUtil;
import com.hendisantika.springbootsecuritythymeleaf.validator.FieldsEqual;
import com.hendisantika.springbootsecuritythymeleaf.validator.UniqueValue;
import com.hendisantika.springbootsecuritythymeleaf.validator.ValidFile;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-security-thymeleaf
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 05/05/20
 * Time: 20.40
 */
@FieldsEqual(firstField = "password", secondField = "confirmPassword")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDetailForm {

    private Long id;
    @Size(min = 5, max = 10)
    @UniqueValue(fieldName = "userName")
    private String userName;
    @Size(min = 5, max = 10)
    private String password;
    @Size(min = 5, max = 10)
    private String confirmPassword;
    @Size(min = 1, max = 10)
    private String firstName;
    @Size(min = 1, max = 10)
    private String lastName;
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    @NotNull
    @Past
    private Date dateOfBirth;
    @NotNull
    private DBEnumUtil.Gender gender;
    @NotEmpty
    @Email
    private String email;
    @ValidFile(contentType = {"image/png", "image/jpeg"}, size = 150)
    private MultipartFile imageFile;
    private Role role;

}
